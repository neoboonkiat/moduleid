﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace OrderList
{
    class Program
    {
        static void Main(string[] args)
        {
            //string input = @"C:\Users\boonkiat.intern\Desktop\Assignment2";
            //string output = @"C:\Users\boonkiat.intern\Desktop\sol.txt";
            Console.WriteLine("Folder input: {0}", args[0]);
            Console.WriteLine("File input: {0}", args[1]);
            Console.WriteLine();
            getOrderList(args[0], args[1], args[2]);


        }
        static void getOrderList(string input, string output, string order)
        {

            if (File.Exists(output))
            {
                File.Delete(output);
            }

            List<string> orderList = new List<string>();
            Dictionary<string, string> orderMap = new Dictionary<string, string>();

            using (StreamReader sr = new StreamReader(order))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    orderList.Add(line);
                    orderMap.Add(line, null);
                }
            }
            
            foreach (string file in Directory.EnumerateFiles(input, "*.xml"))
            {
                XmlTextReader xmlread = new XmlTextReader(file);
                string key = "", val = "";
                while (xmlread.Read())
                {
                    if (xmlread.NodeType == XmlNodeType.Element && xmlread.Name == "FileName" && val == "")
                    {
                        string read = xmlread.ReadElementContentAsString();
                        string[] result = read.Split(new string[] { "_v", "_V" }, StringSplitOptions.None);
                        val = result[0];
                    }

                    if (xmlread.NodeType == XmlNodeType.Element && xmlread.Name == "Name" && key == "")
                    {
                        string read = xmlread.ReadElementContentAsString();
                        key = read;
                    }

                    if (key != "" && val != "")
                        break;
                }

                if (key != "")
                {
                    if (orderMap.ContainsKey(key))
                        orderMap[key] = val;
                    else
                        orderMap.Add(key, val);
                }
            }

            using (StreamWriter sw = new StreamWriter(output))
            {
                {
                    foreach (string s in orderList)
                    {
                        if (orderMap.ContainsKey(s))
                        {
                            sw.WriteLine(orderMap[s]);
                            Console.WriteLine(orderMap[s]);
                            orderMap.Remove(s);
                        }
                    }

                    foreach (KeyValuePair<string, string> p in orderMap)
                    {
                        sw.WriteLine(p.Value);
                    }
                }
            }

#if DISABLE
            using (StreamWriter sw = new StreamWriter(output))
            {
                {
                    foreach (string file in Directory.EnumerateFiles(input, "*.xml"))
                    {
                        XmlTextReader xmlread = new XmlTextReader(file);
                        while (xmlread.Read())
                        {
                            if (xmlread.NodeType == XmlNodeType.Element && xmlread.Name == "FileName")
                            {
                                string read = xmlread.ReadElementContentAsString();
                                string[] result = read.Split(new string[] { "_v", "_V" }, StringSplitOptions.None);
                                sw.WriteLine(result[0]);
                                Console.WriteLine(result[0]);
                            }
                        }
                    }
                }
            }
#endif
            Console.WriteLine();
            Console.WriteLine("Successfully exported order list to : {0}", output);
        }
    }
}
