﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace getModuleID
{
    class Program
    {
        static void Main(string[] args)
        {

            getID(args[0], args[1]);

        }

        static void getID(string filein , string fileout)
        {
            StreamReader reader = new StreamReader(filein);
            using (reader)
            {
                int num = 0;
                int count = 0;
                string line;
                string output = "";
                line = reader.ReadLine();

                while (line != null)

                {

                    string[] result = line.Split(new string[] { " " }, StringSplitOptions.None);
                    if(Int32.TryParse(result[0] , out num))
                    {

                        if (count == 1)
                        {
                            output = result[0];
                        }
                        else
                        {
                            output += "," + result[0];
                        }
                    }
                   
                    line = reader.ReadLine();
                    count++;

                }

                
                //Check if file already exists. If yes, delete it.
                if (File.Exists(fileout))
                {
                    File.Delete(fileout);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileout))
                {
                    Console.WriteLine();
                    Console.WriteLine(output);
                    sw.WriteLine(output);
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Successfully exported to: {0}", fileout);
                }
            }
        }
    }
}
