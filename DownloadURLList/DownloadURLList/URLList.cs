﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace DownloadURLList
{
    class URLList
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Folder input: {0}", args[0]);
            Console.WriteLine("File input: {0}", args[1]);
            Console.WriteLine();
            getDownloadURL(args[0], args[1]);
        }

        static void getDownloadURL(string input, string output)
        {

            if (File.Exists(output))
            {
                File.Delete(output);
            }

            using (StreamWriter sw = new StreamWriter(output))
            {
                {
                    foreach (string file in Directory.EnumerateFiles(input, "*.xml"))
                    {
                        XmlTextReader xmlread = new XmlTextReader(file);
                        while (xmlread.Read())
                        {
                            if (xmlread.NodeType == XmlNodeType.Element && xmlread.Name == "DownloadURL")
                            {
                                string read = xmlread.ReadElementContentAsString();
                                sw.WriteLine(read);
                                Console.WriteLine(read);
                            }
                        }
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Exported URL List to : {0}", output);
        }
    }
}
